<?php

class BreweryDBAPI {
  const BASE_URL = 'http://api.brewerydb.com/v2';
  protected $api_key, $url;

  public function __construct() {
    $this->api_key = variable_get('brewery_db_api_key', NULL);
    $this->url = self::BASE_URL;

    // No API key, there is nothing that can be done.
    if (!isset($this->api_key)) {
      throw new BreweryDBAPIException(BreweryDBAPIException::INVALID_API_KEY);
    }
  }

  /**
   * Simple method to build and return a HttpClient response to a BreweryDB endpoint.
   * @param $endpoint
   * @param array $arguments
   * @param $method
   * @param $data
   * @return bool
   */
  public function request($endpoint, array $arguments = array(), $method = 'GET', $data = NULL) {
    try {
      $arguments['key'] = $this->api_key;
      $arguments = array_reverse($arguments, TRUE);
      $endpoint = url($this->url . $endpoint, array('query' => $arguments));
      $response = drupal_http_request($endpoint, array('method' => $method));
      return json_decode($response->data);
    } catch (Exception $error) {
      watchdog('brewery_db', 'HTTP CALL (@method : @endpoint) ERROR - @error', array('@method' => $method, '@endpoint' => $endpoint, '@error' => $error->getMessage()), WATCHDOG_CRITICAL);
      return FALSE;
    }
  }
}

/**
 * Custom error handler class. Plan for the future.
 */
class BreweryDBAPIException extends Exception {
  const INVALID_API_KEY = 100;
  // @todo: define other possible API errors
  //const OTHER_ERROR = 101;  ... etc

  public function __construct($error_code = 0) {
    parent::__construct(BreweryDBAPIException::getErrorMessage($error_code), $error_code);
  }

  public static function getErrorMessage($error_code) {
    switch($error_code) {
      case self::INVALID_API_KEY:
        return 'The API key is invalid. Please check that one is set in the BreweryDB module configuration settings.';
        break;
      default:
        return 'An unexpected/undefined error has occurred.';
        break;
    }
  }
}