<?php

/**
 * Admin settings form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function brewery_db_admin_settings($form, $form_state) {
  $form['brewery_db_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('You need an !key before you can utilize the API. !signup on BreweryDB and submit your application to gain an API key.', array('!key' => l('API key', 'http://www.brewerydb.com/developers/apps', array('attributes' => array('target' => '_blank'))), '!signup' => l('Signup', 'http://www.brewerydb.com/developers/apps', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => variable_get('brewery_db_api_key', NULL),
  );

  return system_settings_form($form);
}