<?php

/**
 * Implements hook_entity_property_info().
 */
function brewery_db_beer_entity_property_info() {
  $info = array();
  $properties = &$info['brewery_db_beer']['properties'];

  $properties['id'] = array(
    'label' => t('Id'),
    'type' => 'integer',
    'description' => t('The unique ID of the BreweryDB Beer.'),
    'schema field' => 'id',
  );

  $properties['beer_id'] = array(
    'label' => t('Beer ID'),
    'type' => 'text',
    'description' => t('The ID of the beer in the BreweryDB database.'),
    'schema field' => 'beer_id',
  );

  $properties['name'] = array(
    'label' => t('Name'),
    'type' => 'text',
    'description' => t('The name of the beer.'),
    'schema field' => 'name',
  );

  $properties['status'] = array(
    'label' => t('Status'),
    'type' => 'text',
    'description' => t('The status identifier used by BreweryDB.'),
    'schema field' => 'status',
  );

  $properties['status_display'] = array(
    'label' => t('Status Display'),
    'type' => 'text',
    'description' => t('The status label used to display the status property from BreweryDB.'),
    'schema field' => 'status_display',
  );

  $properties['created'] = array(
    'label' => t('Created'),
    'type' => 'date',
    'description' => t('The date the beer entity was created in this system.'),
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Changed'),
    'type' => 'date',
    'description' => t('The date the beer entity was last changed in this system.'),
    'schema field' => 'changed',
  );

  return $info;
}