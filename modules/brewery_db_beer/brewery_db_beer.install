<?php

/**
 * Implements hook_schema().
 */
function brewery_db_beer_schema() {
  $schema = array();

  $schema['cache_brewery_db_beer'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_brewery_db_beer']['description'] = 'Cache table for brewery_db';

  $schema['brewery_db_beer'] = array(
    'description' => 'Stores Beer entities for BreweryDB.',
    'fields' => array(
      'id' => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
      'beer_id' => array('type' => 'varchar', 'length' => 64, 'not null' => TRUE, 'default' => ''),
      'name' => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
      'status' => array('type' => 'varchar', 'length' => 64, 'not null' => FALSE, 'default' => ''),
      'status_display' => array('type' => 'varchar', 'length' => 128, 'not null' => FALSE, 'default' => ''),
      'created' => array('type' => 'int', 'not null' => TRUE),
      'changed' => array('type' => 'int', 'not null' => TRUE),
      'data' => array('type' => 'blob', 'not null' => FALSE, 'size' => 'big', 'serialize' => TRUE, 'description' => 'A serialized array of additional data.',),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'brewery_db_beer_index' => array('id'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function brewery_db_beer_install() {
  if (!taxonomy_vocabulary_machine_name_load('brewerydb_beer_categories')) {
    $info = array(
      'name' => 'BreweryDB Beer Categories',
      'machine_name' => 'brewerydb_beer_categories',
      'description' => t('Categories from the BreweryDB system.'),
      'module' => 'taxonomy',
    );

    $vocabulary = (object) $info;
    taxonomy_vocabulary_save($vocabulary);
  }
}