<?php

class BreweryDBAPIBeer extends BreweryDBAPI {
  public function __construct(array $values = array()) {
    parent::__construct();
  }

  /**
   * Gets a list of beers.
   * At least one parameter is required unless the API account is flagged as Premium, so we pass availableId.
   * @param $page_number
   * @return bool
   */
  public function getBeers($page_number = 1) {
    return $this->request('/beers', array('availableId' => 1, 'p' => $page_number));
  }

  /**
   * Get information for one beer. Optionally return breweries associated to the beer.
   * @param $beer_id
   * @param string $with_breweries
   * @return bool
   */
  public function getBeer($beer_id, $with_breweries = 'N') {
    return $this->request('/beer/' . $beer_id, array('withBreweries' => $with_breweries));
  }

  /**
   * Performs a lookup by beer name.
   * @param $name
   * @return bool
   */
  public function getBeerByName($name) {
    return $this->request('/beers', array('name' => $name));
  }

  /**
   * Get events associated to this beer. Optionally return only events the beer won awards at.
   * @param $beer_id
   * @param string $only_winners
   * @return bool
   */
  public function getBeerEvents($beer_id, $only_winners = 'N') {
    return $this->request('/beer/' . $beer_id . '/events', array('onlyWinners' => $only_winners));
  }

  /**
   * @fixme: this endpoint is either currently broken or requires authorization.
   * Get a list of ingredients for a beer.
   * @param $beer_id
   * @return bool
   */
  public function getBeerIngredients($beer_id) {
    return $this->request('/beer/' . $beer_id . '/ingredients');
  }

  /**
   * Get a random beer.
   * @param string $has_labels
   * @param string $with_breweries
   * @return bool
   */
  public function getRandomBeer($has_labels = 'Y', $with_breweries = 'Y') {
    return $this->request('/beer/random', array('hasLabels' => $has_labels, 'withBreweries' => $with_breweries));
  }

  /**
   * Get a list of beer categories.
   * @return bool
   */
  public function getBeerCategories() {
    return $this->request('/categories');
  }

  /**
   * Get a list of styles, sub-categories of beer.
   * @return bool
   */
  public function getBeerStyles() {
    return $this->request('/styles');
  }
}