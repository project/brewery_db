<?php

class BreweryDBBeerController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create and return a new BreweryDBBeer entity.
   * @param array $values
   * @return object
   */
  public function create(array $values = array()) {
    $values += array(
      'beer_id' => '',
      'name' => '',
      'status' => '',
      'status_display' => '',
      'created' => REQUEST_TIME,
      'data' => '',
    );

    return parent::create($values);
  }

  /**
   * Save a BreweryDBBeer Entity.
   * @param $entity
   * @param DatabaseTransaction $transaction
   * @return bool|int
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->changed = REQUEST_TIME;

    if (empty($entity->{$this->idKey}) || !empty($entity->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($entity->created)) {
        $entity->created = REQUEST_TIME;
      }
    }

    return parent::save($entity, $transaction);
  }

  public function view($entities, $view_mode = '', $langcode = NULL, $page = NULL) {
    return parent::view($entities, $view_mode = '', $langcode = NULL, $page = NULL);
  }
}