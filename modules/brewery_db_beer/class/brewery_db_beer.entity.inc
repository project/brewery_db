<?php

class BreweryDBBeer extends Entity {
  public function __construct(array $values = array()) {
    parent::__construct($values, 'brewery_db_beer');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  public function label() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'brewery_db_beer/' . $this->id);
  }

  public function path() {
    $uri = $this->uri();
    return $uri['path'];
  }

  public function url() {
    $uri = $this->uri();
    return url($uri['path'], $uri);
  }

  public function findByBeerId($beer_id) {

  }
}

class BreweryDBBeerQuery extends EntityFieldQuery {
  public function __construct(array $values = array()) {
    $this->entityCondition('entity_type', $values['entity_type']);
    $this->pager();
  }

  public function findByBeerId($beer_id) {
    $this->propertyCondition('beer_id', $beer_id);
    $result = $this->execute();

    if ($result) {
      return brewery_db_beer_load(array_keys($result['brewery_db_beer']));
    }

    return FALSE;
  }

  public function findByName($name) {
    $this->propertyCondition('name', $name);
    $result = $this->execute();

    if ($result) {
      return brewery_db_beer_load(array_keys($result['brewery_db_beer']));
    }

    return FALSE;
  }
}