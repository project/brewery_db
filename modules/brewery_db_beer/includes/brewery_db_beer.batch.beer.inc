<?php

/**
 * Batch update form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function brewery_db_beer_batch_import_beer_form($form, &$form_state) {
  return confirm_form($form, 'Import Beer', 'admin/config/services/brewery-db/beer', t('<p>This will import beer from BreweryDB using the !getBeers method into your system and create/update Beer entities.</p>', array('!getBeers' => l('getBeers()', 'http://www.brewerydb.com/developers/docs-endpoint/beer_index#1', array('external' => TRUE, 'attributes' => array('target' => '_blank'))))), 'Import Beer', 'Cancel');
}

/**
 * Form submit handler. Sets up the batch.
 * @param $form
 * @param $form_state
 */
function brewery_db_beer_batch_import_beer_form_submit($form, &$form_state) {
  $client = new BreweryDBAPIBeer();
  $response = $client->getBeers();

  $start = 1;
  $end = $response->numberOfPages;
  $operations = array();

  foreach ($response->data as $beer) {
    $operations[] = array('brewery_db_beer_batch_process_beer', array($beer));
  }

  /*while ($start < $end) {
    if (!isset($response)) {
      $client->getBeers($start);
    }

    foreach ($response->data as $beer) {
      $operations[] = array('brewery_db_beer_batch_process_beer', array($beer));
    }

    unset($response);
    $start++;
  }*/

  $batch = array(
    'title' => t('Bartender!! Please pass down some beers from BreweryDB!'),
    'init_message' => t('Pouring some pints...'),
    'operations' => $operations,
    'finished' => 'brewery_db_beer_import_beer_batch_finished',
    'file' => drupal_get_path('module', 'brewery_db_beer') . '/includes/brewery_db_beer.batch.beer.inc',
  );

  batch_set($batch);
  $form_state['redirect'] = 'admin/config/services/brewery-db/beers/import';
}

/**
 * This processes a beer item from BreweryDB.
 * @param $item
 * @param $context
 */
function brewery_db_beer_batch_process_beer($item, &$context) {
  // @todo: abstract this out into a class method to reduce possible code duplication
  // @todo: a Feature will provide a fully populated/Fielded entity so we need mapper methods anyway.

  $query = new BreweryDBBeerQuery(array('entity_type' => 'brewery_db_beer'));
  $beer = $query->findByBeerId($item->id);

  if (!$beer) {
    $beer = entity_create('brewery_db_beer', array());
    $beer->is_new = TRUE;
  }

  $beer->beer_id = $item->id;
  $beer->name = $item->name;
  $beer->status = $item->status;
  $beer->status_display = $item->statusDisplay;
  $beer->data = serialize($item);

  entity_get_controller('brewery_db_beer')->save($beer);

  $context['message'] = t('Imbibing some @name', array('@name' => $item->name));
  $context['results'][] = $item->id;
}

/**
 * Batch finished callback.
 * @param $success
 * @param $results
 * @param $operations
 */
function brewery_db_beer_import_beer_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = count($results) .' bottles of beer on the wall, ' . count($results) . ' bottles of beer. You take a few down and pass them arounndnmerrgg,,m.. *floor*.';
    $message .= theme('item_list', $results);
    variable_set('brewery_db_beer_last_sync', REQUEST_TIME);
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}