<?php

/**
 * Batch update form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function brewery_db_beer_batch_import_category_form($form, &$form_state) {
  return confirm_form($form, 'Import Categories', 'admin/config/services/brewery-db/beer', t('<p>This will import beer categories from BreweryDB using the !getCategories method into your system and create/update taxonomy.</p>', array('!getCategories' => l('getCategories()', 'http://www.brewerydb.com/developers/docs-endpoint/category_index', array('external' => TRUE, 'attributes' => array('target' => '_blank'))))), 'Import Categories', 'Cancel');
}

/**
 * Form submit handler. Sets up the batch.
 * @param $form
 * @param $form_state
 */
function brewery_db_beer_batch_import_category_form_submit($form, &$form_state) {
  if (!taxonomy_vocabulary_machine_name_load('brewerydb_beer_categories')) {
    $info = array(
      'name' => 'BreweryDB Beer Categories',
      'machine_name' => 'brewerydb_beer_categories',
      'description' => t('Categories from the BreweryDB system.'),
      'module' => 'taxonomy',
    );

    $vocabulary = (object) $info;
    taxonomy_vocabulary_save($vocabulary);
  }

  $client = new BreweryDBAPIBeer();

  // Gather parent categories
  $response = $client->getBeerCategories();

  foreach ($response->data as $category) {
    $operations[] = array('brewery_db_beer_batch_process_category', array($category));
  }

  // Gather 'styles' and set as children of parent terms
  $response = $client->getBeerStyles();

  foreach ($response->data as $category) {
    $operations[] = array('brewery_db_beer_batch_process_style', array($category));
  }

  $batch = array(
    'title' => t('Import all sorts of categories from BreweryDB'),
    'init_message' => t('Doing some smart stuff...'),
    'operations' => $operations,
    'finished' => 'brewery_db_beer_import_category_batch_finished',
    'file' => drupal_get_path('module', 'brewery_db_beer') . '/includes/brewery_db_beer.batch.category.inc',
  );

  batch_set($batch);
  $form_state['redirect'] = 'admin/config/services/brewery-db/beers/import-categories';
}

/**
 * This processes a beer category from BreweryDB.
 * @param $item
 * @param $context
 */
function brewery_db_beer_batch_process_category($item, &$context) {
  $term = end(taxonomy_get_term_by_name($item->name, 'brewerydb_beer_categories'));

  if (!$term) {
    $term = new stdClass();
  }

  $term->name = $item->name;
  $term->vid = taxonomy_vocabulary_machine_name_load('brewerydb_beer_categories')->vid;
  taxonomy_term_save($term);

  $context['message'] = t('Creating "@category" category', array('@category' => $item->name));
  $context['results'][] = $item->id;
}

/**
 * This processes a style from BreweryDB.
 * @param $item
 * @param $context
 */
function brewery_db_beer_batch_process_style($item, &$context) {
  $parent = end(taxonomy_get_term_by_name($item->category->name, 'brewerydb_beer_categories'));
  $term = end(taxonomy_get_term_by_name($item->name, 'brewerydb_beer_categories'));

  if (!$term) {
    $term = new stdClass();
  }

  $term->name = $item->name;
  $term->vid = taxonomy_vocabulary_machine_name_load('brewerydb_beer_categories')->vid;
  $term->description = $item->description;
  $term->parent = $parent->tid;
  taxonomy_term_save($term);

  $context['message'] = t('Creating "@category" category', array('@category' => $item->name));
  $context['results'][] = $item->id;
}

/**
 * Batch finished callback.
 * @param $success
 * @param $results
 * @param $operations
 */
function brewery_db_beer_import_category_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = count($results) .' categories created/updated.';
    $message .= theme('item_list', $results);
    variable_set('brewery_db_beer_last_category_sync', REQUEST_TIME);
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}