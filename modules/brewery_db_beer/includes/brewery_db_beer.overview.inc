<?php

/**
 * Page callback for the main Beer page.
 * @return string
 */
function brewery_db_beer_overview() {
  return t('A default view will go here eventually, displaying a table of beer entities. Here you can manage the Beer entity, import data, import categories, or manage fields on the Beer entity.');
}